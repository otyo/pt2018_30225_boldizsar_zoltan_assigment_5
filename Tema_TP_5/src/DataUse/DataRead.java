package DataUse;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;

import DataMonitoring.MoniToredData;

public interface DataRead<S>
{
	String[] map(String source);
	public static ArrayList<String[]> mapString(String in, DataRead<?> mapper) throws Exception 
	{
		String line;
		ArrayList<String[]> rezultat=new ArrayList<String[]> ();
		String[] temp;
		FileReader fileReader = new FileReader(in);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        while((line = bufferedReader.readLine()) != null) 
        {
        	
        	temp=mapper.map(line);
        	rezultat.add(temp);
        }
		// returns the int array of mapped values
        bufferedReader.close();  
		return rezultat;
	}
	//(String[] parts)->line.split("\t");
}

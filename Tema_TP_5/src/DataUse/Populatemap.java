package DataUse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import DataMonitoring.MoniToredData;

public interface Populatemap<S>
{
	Integer get(Map<String,Integer> activityMap,MoniToredData data);
	public static Map<String,Integer> createmap(ArrayList<MoniToredData> in,Populatemap<?> mapper) throws Exception 
	{
		Map<String,Integer> activityMap;
		 activityMap=new HashMap<String,Integer>();
		 Integer i;
		 for(MoniToredData data:in)
		 {
			 
			 i=mapper.get(activityMap, data);
			 if(i==null)
			 {
				 activityMap.put(data.getActivity(),1);
			 }
			 else
			 {
				 activityMap.remove(data.getActivity());
				 i++;
				 activityMap.put(data.getActivity(),i);
			 }
		 }
        return activityMap;
	}
}

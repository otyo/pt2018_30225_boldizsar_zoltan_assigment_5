package DataUse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import DataMonitoring.MoniToredData;

public interface PopulateCalc<S>
{
	Integer get(Map<String,Integer> activityMap,MoniToredData data);
	public static Map<String,Integer> createmap(ArrayList<MoniToredData> in,int[] datain,PopulateCalc<?> mapper) throws Exception 
	{
		Map<String,Integer> activityMap;
		 activityMap=new HashMap<String,Integer>();
		 Integer i;
		 for(int a=0;a<in.size();a++)
		 {
			 i=mapper.get(activityMap,in.get(a));
			 if(i==null)
			 {
				 activityMap.put(in.get(a).getActivity(),datain[a]);
			 }
			 else
			 {
				 activityMap.remove(in.get(a).getActivity());
				 i+=datain[a];
				 activityMap.put(in.get(a).getActivity(),i);
			 }
		 }
        return activityMap;
	}
}

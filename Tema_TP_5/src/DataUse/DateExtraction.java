package DataUse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import DataMonitoring.MoniToredData;

public interface DateExtraction<S>
{
	String[] get(String source);
	public static int[] createmap(ArrayList<MoniToredData> in,DateExtraction<?> mapper) throws Exception 
	{
		int[] rezultat=new int[in.size()];
		int temp;
		String[] a1,a2;
		for(int i=0;i<in.size();i++)
		{
			a1=mapper.get(in.get(i).getStart());
			a2=mapper.get(in.get(i).getFinish());
			rezultat[i]=(Integer.parseInt(a2[0])-Integer.parseInt(a1[0]))*365*24*60*60+(Integer.parseInt(a2[1])-Integer.parseInt(a1[1]))*30*24*60*60+(Integer.parseInt(a2[2])-Integer.parseInt(a1[2]))*24*60*60+(Integer.parseInt(a2[3])-Integer.parseInt(a1[3]))*60*60+(Integer.parseInt(a2[4])-Integer.parseInt(a1[4]))*60+(Integer.parseInt(a2[5])-Integer.parseInt(a1[5]));
		}
		return rezultat;
	}
}

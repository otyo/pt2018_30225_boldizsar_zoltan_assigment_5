package DataUse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import DataMonitoring.MoniToredData;

public interface Calc<S>
{
	Integer get(Map<String,Integer> activityMap,MoniToredData data);
	public static Map<String,Integer> createmap(ArrayList<MoniToredData> in,int[] datain,Calc<?> mapper) throws Exception 
	{
		Map<String,Integer> activityMap;
		 activityMap=new HashMap<String,Integer>();
		 Integer i;
		 for(int a=0;a<in.size();a++)
		 {
			
			 i=mapper.get(activityMap,in.get(a));
			 if(i==null)
			 {
				 if(datain[a]<=300)
				 {
				 activityMap.put(in.get(a).getActivity(),1);
				 }
				 else
				 {
					 activityMap.put(in.get(a).getActivity(),0);
				 }
			 }
			 else
			 {
				 if(datain[a]<=300)
				 {
				 activityMap.remove(in.get(a).getActivity());
				 i++;
				 activityMap.put(in.get(a).getActivity(),i);
				 }
			 }
		 }
        return activityMap;
	}
}

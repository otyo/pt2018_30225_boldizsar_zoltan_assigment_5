package DataUse;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.swing.text.html.HTMLDocument.Iterator;

import DataMonitoring.MoniToredData;

public class DataUsage 
{
	 ArrayList<MoniToredData> mydata;
	 Map<String,Integer> activityMap,totaltime,sub5;
	 Map<String,Date> activitydate;
	 Map<Integer,Map<String,Integer>> dailyactivityMap;
	 int[] times;
	 public DataUsage()
	 {
		 mydata=new ArrayList<MoniToredData>();
		 dailyactivityMap=new HashMap<Integer,Map<String,Integer>>();
	 }
	 public boolean read1(String fileName)
	 {
		 boolean rezultat=true;
		 ArrayList<String[]> intermediar=new ArrayList<String[]>();
		 try {
			intermediar=DataRead.mapString(fileName,(String souce)->souce.split("\t"));
		} catch (Exception e) {
			rezultat=false;
			System.out.println(e.getMessage());
		}
		 int i;
		 for(String[] str:intermediar)
		 {
			 if(str.length>1)
			 {
				 mydata.add(new MoniToredData(str[0],str[2],str[4]));
			 }
		 }
		 return rezultat;
	 }
	 public int countDays1()
	 {
		 int rezultat=0;
		try {
			rezultat = Sameday.countDays(mydata,(String source1,String source2)->{
				int i;
				String[] separateday1,separateday2,timetocompare1,timetocompare2;
				int[] date1,date2;
				separateday1=source1.split(" ");
				separateday2=source2.split(" ");
				timetocompare1=separateday1[0].split("-");
				timetocompare2=separateday2[0].split("-");
				date1=new int[timetocompare1.length];
				for(i=0;i<timetocompare1.length;i++)
				{
					date1[i]=Integer.parseInt(timetocompare1[i]);
					//System.out.println(date1[i]);
				}
				date2=new int[timetocompare2.length];
				for(i=0;i<timetocompare2.length;i++)
				{
					date2[i]=Integer.parseInt(timetocompare2[i]);
					//System.out.println(date2[i]);
				}
				if(date1[0]==date2[0]&&date1[1]==date2[1]&&date1[2]==date2[2]) return true;
				else return false;
				}
			 );
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 return rezultat;
	 }
	 public void populateMap1()
	 {
		 try {
			this.activityMap=Populatemap.createmap(mydata,(Map<String,Integer> activityMap,MoniToredData data)->
			 {
				 return activityMap.get(data.getActivity());
			 }
			 );
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	 }
	 public void writeMap()
	 {
		 try
		 {
			 Set<String> mapKeys=activityMap.keySet();
			 String temp;
			 java.util.Iterator<String> myiterator=mapKeys.iterator();
			 BufferedWriter writer = new BufferedWriter(new FileWriter("totalout.txt"));
			 while(myiterator.hasNext())
			 {
				 temp=myiterator.next();
				 writer.write(temp+" "+activityMap.get(temp)+System.lineSeparator());
			 }
			 writer.close();
		 }
		 catch(Exception e)
		 {
			 System.out.println("Failure"); 
		 }
	 }
	 public void dailyActivityMap()
	 {
		 ArrayList<MoniToredData> actual=new ArrayList<MoniToredData>();
		 MoniToredData last=null;
		 int ziua=1,temp;
		 int activitate=0;
		 boolean re=false,lastz=false;
		 temp=this.countDays1();
		 while(ziua<=temp)
		 {
			 if(re) actual=new ArrayList<MoniToredData>();
			 re=false;
			 if(ziua==1&&activitate==0) 
				 {
				  actual.add(mydata.get(0));
				  last=mydata.get(0);
				  activitate++;
				 }
			 else
			 {
				 
				 actual.add(mydata.get(activitate));
				  last=mydata.get(activitate);
				  activitate++;
			 }
			 
			 //if(activitate!=mydata.size())
			 //System.out.println(last.getFinish()+" "+mydata.get(activitate).getStart());
			 if(activitate==mydata.size())
			 {
				 lastz=true;
				 activitate=0;
			 }
			
			 if(!last.sameDay(mydata.get(activitate).getStart())||lastz||!last.sameDay(last.getStart())) 
			 {
				 try {
					
						
					dailyactivityMap.put(ziua,Populatemap.createmap(actual,(Map<String,Integer> activityMap,MoniToredData data)->
					 {
						 return activityMap.get(data.getActivity());
					 }
					 ));
					
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				 ziua++;
				 re=true;
			 }
		 }
		 writeMap1(this.dailyactivityMap);
	 }
	 public void writeMap1(Map<Integer,Map<String,Integer>> Map2)
	 {
		 Map<String,Integer> Map1=new HashMap<String,Integer>();
		 Set<String> mapKeys=null;
		 String temp;
		 java.util.Iterator<String> myiterator=null;
		 BufferedWriter writer=null;
		try {
			writer = new BufferedWriter(new FileWriter("dailytotalout.txt"));
		
		 for(int i=1;i<=this.countDays1();i++)
		 {
			 Map1=Map2.get(i);
		 try
		 {
			 mapKeys=Map1.keySet();
			 myiterator=mapKeys.iterator();
			 while(myiterator.hasNext())
			 {
				 temp=myiterator.next();
				 writer.write(i+" "+temp+" "+Map1.get(temp)+System.lineSeparator());
			 }
			 
		 }
		 catch(Exception e)
		 {
			 System.out.println("Failure2"); 
		 }
		 }
		 writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		};
	 }
	 public void calculate()
	 {
		 try {
			times=DateExtraction.createmap(mydata,(source)->
			 {
				String[] rezultat;
				 String[] date=null,date1,date2;
				 date=source.split(" ");
				 date1=date[0].split("-");
				 date2=date[1].split(":");
				 rezultat=new String[date1.length+date2.length];
				 for(int i=0;i<date1.length;i++) rezultat[i]=date1[i];
				 for(int i=date1.length;i<date1.length+date2.length;i++) rezultat[i]=date2[i-date1.length];
				 return rezultat;
			 }
			 );
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	 }
	 public void calculatedaily()
	 {
		 try {
			times=DateExtraction.createmap(mydata,(source)->
			 {
				String[] rezultat;
				 String[] date=null,date1,date2;
				 date=source.split(" ");
				 date1=date[0].split("-");
				 date2=date[1].split(":");
				 rezultat=new String[date1.length+date2.length];
				 for(int i=0;i<date1.length;i++) rezultat[i]=date1[i];
				 for(int i=date1.length;i<date1.length+date2.length;i++) rezultat[i]=date2[i-date1.length];
				 return rezultat;
			 }
			 );
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	 }
	 public void calcactivity()
	 {
		 try {
			totaltime=PopulateCalc.createmap(mydata, times,
			 (Map<String,Integer> activityMap,MoniToredData data)->
			 {
				 return activityMap.get(data.getActivity());
			 }
			 );
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 String datestring = null;
		 SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		 java.util.Date date;
		 for(String a:totaltime.keySet())
		 {
			
			 datestring=totaltime.get(a)/31536000+"/"+totaltime.get(a)%31536000/2592000+"/"+totaltime.get(a)%2592000/86400+" "+totaltime.get(a)%86400/3600+":"+totaltime.get(a)%3600/60+":"+totaltime.get(a)%60;
			 try {
					date = format.parse(datestring);
					 System.out.println(a+" "+totaltime.get(a)/31536000+"/"+totaltime.get(a)%31536000/2592000+"/"+totaltime.get(a)%2592000/86400+" "+totaltime.get(a)%86400/3600+":"+totaltime.get(a)%3600/60+":"+totaltime.get(a)%60);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		 }
		
		 
	 }
	 public void calc()
	 {
		 try {
				sub5=Calc.createmap(mydata, times,
				 (Map<String,Integer> activityMap,MoniToredData data)->
				 {
					 return activityMap.get(data.getActivity());
				 }
				 );
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		 for(String a:sub5.keySet())
		 {
			 if((float)sub5.get(a)/activityMap.get(a)>0.9)
			 System.out.println(a);
		 }
	 }
	 public static void main(String [] args) 
	 {
		 int i;
		
           DataUsage myuse=new DataUsage();
	       if(myuse.read1("Activity.txt"))
	       {
	    	  System.out.println("Succes"); 
	    	  System.out.println(myuse.countDays1()); 
	    	  myuse.populateMap1();
	    	  myuse.writeMap();
	    	  myuse.dailyActivityMap();
	    	  myuse.calculate();
	    	  myuse.calcactivity();
	    	  myuse.calc();
	       }
	       else
	       {
	    	   System.out.println("Failure"); 
	       }
	    }
}

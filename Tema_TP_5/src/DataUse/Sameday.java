package DataUse;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;

import DataMonitoring.MoniToredData;

public interface Sameday<S>
{
	boolean count(String source1,String source2);
	public static int countDays(ArrayList<MoniToredData> in, Sameday<?> mapper) throws Exception 
	{
		
        int rezultat=1;
        for(int i=0;i<in.size();i++)
		 {
			if(!mapper.count(in.get(i).getStart(),in.get(i).getFinish())) rezultat++;
			if(i!=in.size()-1)
			{
			 if(!mapper.count(in.get(i).getFinish(),in.get(i+1).getStart())) rezultat++;
			}
		 }
        return rezultat;
	}
	//(String[] parts)->line.split("\t");
}

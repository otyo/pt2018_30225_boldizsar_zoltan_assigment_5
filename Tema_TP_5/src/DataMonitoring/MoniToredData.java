package DataMonitoring;

import java.io.BufferedReader;
import java.io.FileReader;

public class MoniToredData
{
	private String start_time,end_time,activity;
	public MoniToredData(String in_start_time,String in_end_time,String in_activity)
	{
		start_time=in_start_time;
		end_time=in_end_time;
		activity=in_activity; 
	}
	public boolean sameDay(String testTime)
	{
		boolean rezultat=false;
		int i;
		String[] separateday1,separateday2,timetocompare1,timetocompare2;
		int[] date1,date2;
		separateday1=this.end_time.split(" ");
		separateday2=testTime.split(" ");
		timetocompare1=separateday1[0].split("-");
		timetocompare2=separateday2[0].split("-");
		date1=new int[timetocompare1.length];
		for(i=0;i<timetocompare1.length;i++)
		{
			date1[i]=Integer.parseInt(timetocompare1[i]);
			//System.out.println(date1[i]);
		}
		date2=new int[timetocompare2.length];
		for(i=0;i<timetocompare2.length;i++)
		{
			date2[i]=Integer.parseInt(timetocompare2[i]);
			//System.out.println(date2[i]);
		}
		if(date1[0]==date2[0]&&date1[1]==date2[1]&&date1[2]==date2[2]) rezultat=true;
		return rezultat;
	}
	public String getStart()
	{
		return this.start_time;
	}
	public String getFinish()
	{
		return this.end_time;
	}
	public String getActivity()
	{
		return this.activity;
	}
}
